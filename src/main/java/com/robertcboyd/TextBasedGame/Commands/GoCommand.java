package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.*;

/**
 * This Command (or a derivative thereof) should be invoked
 * any time the user is trying to exit the current Place and
 * head to a new one.
 */
public class GoCommand extends Command{

	private Player player;
	private Direction directionToGo;
	private WorldManager wm;
	private UI ui;
	
	/**
	 * Causes the player's location to be set to the destination in accordance with the given cardinal direction or other specifier.
	 */
	@Override
	public void invoke()
	{
		Exit exit = wm.getExit(directionToGo, player.getLocation());

		if(exit != null) {
			exit.onExit(player);
		} else {
			ui.println("You can't go that way.");
		}
	}

	/**
	 * Create a new GoCommand.
	 *
	 * @param player the player to move
	 * @param direction the direction to go
	 * @param wm the WorldManager of the current world
	 * @param ui the UI to print errors to
	 */
	public GoCommand(Player player, Direction direction, UI ui, WorldManager wm)
	{
		this.player = player;
		this.directionToGo = direction;
		this.wm = wm;
		this.ui = ui;
	}
}

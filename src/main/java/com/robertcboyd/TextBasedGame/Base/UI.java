package com.robertcboyd.TextBasedGame.Base;

import java.util.Scanner;

/**
 * Utility class with methods to get and print to the console.
 * Largely wraps System.out and System.in
 */
public class UI {

	private Scanner scanner;

	/**
	 * Asks the user for input in a nice way with a colon for a prompt.
	 *
	 * @return A String with the user's input.
	 */
	public String getInput()
	{
		println();
		println("What would you like to do?");
		print(":");
		String tmp = scanner.nextLine();
		println();
		return tmp;
		//TODO catch scanner errors
	}

	/**
	 * Wraps System.out.print(). Should be used any time one wishes to print information to the user console in a manner like System.out.print().
	 *
	 * @param string The String to be printed.
	 */
	public void print(String string)
	{
		System.out.print(string);
	}

	/**
	 * Wraps System.out.println(). Should be used any time one wishes to print information to the user console in a manner like System.out.println().
	 *
	 * @param string The String to be printed.
	 */
	public void println(String string)
	{
		System.out.println(string);
	}

	/**
	 * Prints a blank line. Simply a shorthand for UI.println("").
	 */
	public void println()
	{
		System.out.println("");
	}

	/**
	 * UI is a utility class that provides convenient methods for
	 * printing and obtaining text from the console.
	 */
	public UI(Scanner scanner)
	{
		this.scanner = scanner;
	}
}

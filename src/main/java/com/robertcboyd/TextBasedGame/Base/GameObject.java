package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;

public abstract class GameObject implements Lookable {
	public abstract boolean isVisibleTo(Player player);
	public abstract ArrayList<String> getIdentifiers();
}
